<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'forum_perime_description' => 'Clôture automatiquement le forum des articles passé une certain nombre de jours ',
	'forum_perime_nom' => 'Forum périmé',
	'forum_perime_slogan' => 'Ferme automatiquement les forums de vieux articles',
);

?>