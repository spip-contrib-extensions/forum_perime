<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_principal' => 'Fermeture automatique des forums publics',
	'cfg_titre_parametrages' => 'Durée de vie des forums ',

	// D
	'duree' => 'En nombre de jours (laisser vider pour désactiver la fonction)',

	// F
	'forum_perime_titre' => 'Forum périmé',

	// L
	'label_duree' => 'Laisser le forum actif :',

	// T
	'titre_page_configurer_forum_perime' => 'Forums: durée des commentaires',
);

?>